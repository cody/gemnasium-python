FROM python:3
RUN pip install pipdeptree
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
